package com.oreillyauto.service;

import com.oreillyauto.domain.Carpart;
import com.oreillyauto.dto.Email;

public interface CarpartsService {
	public Carpart getCarpartByPartNumber(String partNumber);
	public Carpart getCarpart(String partNumber) throws Exception;
	public boolean saveCarpart(Carpart carpart);
	public void deleteCarpartByPartNumber(Carpart carpart);
	public boolean saveCarpart(String partnumber, String title);
	public void sendEmail(Email email) throws Exception;
}
