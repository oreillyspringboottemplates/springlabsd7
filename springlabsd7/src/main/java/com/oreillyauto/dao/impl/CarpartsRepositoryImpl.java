package com.oreillyauto.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CarpartsRepositoryCustom;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.domain.QCarpart;

@Repository
public class CarpartsRepositoryImpl extends QuerydslRepositorySupport implements CarpartsRepositoryCustom {
	
	QCarpart carpartTable = QCarpart.carpart;
	
	public CarpartsRepositoryImpl() {
		super(Carpart.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Carpart> getCarparts() {
		String sql = "SELECT * " + 
	                 "  FROM carparts ";
		Query query = getEntityManager().createNativeQuery(sql);
		return (List<Carpart>) query.getResultList();
	}

	@Override
	public Carpart getCarpartByPartNumber(String partNumber) {
		Carpart carpart = (Carpart) getQuerydsl().createQuery()
			    .from(carpartTable)
			    .where(carpartTable.partNumber.eq(partNumber))
			    .fetchOne();
			return carpart;
	}

	@Override
	public Carpart getCarpart(String partNumber) {
		Carpart carpart = (Carpart) getQuerydsl().createQuery()
		    .from(carpartTable)
		    .where(carpartTable.partNumber.eq(partNumber))
		    .fetchOne();
		return carpart;
	}

	
}

