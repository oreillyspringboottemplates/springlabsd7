package com.oreillyauto.controllers;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.oreillyauto.domain.Carpart;
import com.oreillyauto.dto.Email;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartController {

	@Autowired
	private CarpartsService carpartsService;

	@GetMapping(value = "/carparts/regular")
	public String getRegularForm(Model model, @ModelAttribute("message") String message,
			@ModelAttribute("messageType") String messageType) {
		
		Email email = new Email();
		model.addAttribute("command", email);
		model.addAttribute("active", "regular");
		return "springForm";
	}

	@PostMapping(value = "/carparts/feedback")
    public String postRegularForm(@ModelAttribute("command") Email email, BindingResult result, 
    		Model model, RedirectAttributes ra) {
		
        System.out.println("from=>" + email.getEmailAddress() + " body=>" + email.getEmailBody());
        model.addAttribute("active", "regular");
        
        // THIS BELONGS IN THE SERVICE
        //result.rejectValue(email.getEmailAddress(), "Please fill in your email");
        if (email.getEmailAddress() == null || email.getEmailAddress().length() == 0 || 
        	    email.getEmailBody() == null || email.getEmailBody().length() == 0) {
            // Prepare the form again and then load the view
        	model.addAttribute("messageType", "danger");
            model.addAttribute("message", "Please ensure all fields are populated!");
            return "springForm";
        } else {
        	// Success . Build new form and redirect
        	ra.addFlashAttribute("messageType", "success");
            ra.addFlashAttribute("message", "Success! Thank you for your feedback!");            
            return "redirect:/carparts/regular";
        }                          	  
       
    }

	
	@GetMapping(value = { "/carparts/emailus" })
	public String getEmailUs(Model model) {
		model.addAttribute("active", "emailus");
		return "emailus";
	}

    @ResponseBody
    @PostMapping(value = { "/carparts/emailus" })
    public Email postEmailUs(@RequestBody Email email, HttpResponse resp) {
        try {
        	//resp.setStatusCode(418);
            carpartsService.sendEmail(email);
            email.setMessageType("success");
            email.setMessage("Email Sent Successfully!");
            return email;
            //return Helper.getJson(email);
        } catch (Exception e) {
        	System.out.println(e.getMessage());
        	String error = ((e == null || e.getMessage() == null) ? "Null Pointer Exception" : e.getMessage());
        	email.setMessageType("danger");
        	// Do not send e.getMessage() to users in production
            email.setMessage(error);
            return email;
        }
    }

	@GetMapping(value = { "/carparts/contactus" })
	public String getContactUs(Model model, @ModelAttribute("message") String message, 
			@ModelAttribute("messageType") String messageType) {
		
		model.addAttribute("active", "contactus");
		return "contactus";
	}

	@PostMapping(value = {"/carparts/contactus"})
	public String postContactUs(Model model, Email email, 
			RedirectAttributes ra) throws Exception {
		
		System.out.println("from=>" + email.getEmailAddress() + " body=>" + email.getEmailBody());
		
		// Version 1 
//		return "contactus";
		
		// Version 2
        try {
            carpartsService.sendEmail(email);
            ra.addAttribute("messageType", "success");
            ra.addAttribute("message", "Your message was sent successfully.");
        } catch (Exception e) {
        	ra.addFlashAttribute("messageType", "danger");
        	// Never send e.getMessage() to end users in Production!!!!!!!!!!!
        	ra.addFlashAttribute("message", e.getMessage());
        }
        
        return "redirect:/carparts/contactus";
	}

	@GetMapping(value = { "/carparts" })
	public String getIndex(Model model, String firstName, String lastName) {
		String fullName = ((firstName != null && firstName.length() > 0) ? firstName : "");
		fullName = ((fullName.length() > 0) ? (firstName + " " + lastName) : "");
		model.addAttribute("fullName", fullName);
		return "carparts";
	}

	@GetMapping(value = { "/carparts/other/{partNumber}", "/carparts/engine/{partNumber}",
			"/carparts/electrical/{partNumber}" })
	public String getCarpart(@PathVariable String partNumber, Model model) {
		Carpart carpart = carpartsService.getCarpartByPartNumber(partNumber);
		model.addAttribute("carpart", carpart);
		model.addAttribute("active", partNumber);
		return "carparts";
	}

	@GetMapping(value = { "/carparts/partnumber" })
	public String getPartnumber(Model model) {
		model.addAttribute("active", "add");
		return "partnumber";
	}

	@PostMapping(value = { "/carparts/partnumber" })
	public String postPartnumber(Model model, String partnumber, String title, String update) {

		System.out.println("partnumber=>" + partnumber + " title=>" + title);

		Carpart carpart = new Carpart();
		carpart.setPartNumber(partnumber);
		carpart.setTitle(title);
		carpart.setLine("");
		carpart.setDescription("");

		boolean saved = carpartsService.saveCarpart(partnumber, title);

		String action = ("true".equalsIgnoreCase(update)) ? "Updated" : "Saved";

		if (saved) {
			model.addAttribute("messageType", "success");
			model.addAttribute("message", "Part Number " + partnumber + " " + action + " Successfully");
			model.addAttribute("carpart", carpart);
		} else {
			model.addAttribute("messageType", "danger");
			model.addAttribute("message", "Part Number " + partnumber + " Not " + action + " Successfully!");
		}

		return "partnumber";
	}

	@GetMapping(value = { "/carparts/delete/{partnumber}" })
	public String deleteCarpart(@PathVariable String partnumber, Model model) {

		if (partnumber != null && partnumber.length() > 0) {
			Carpart carpart = carpartsService.getCarpartByPartNumber(partnumber);
			carpartsService.deleteCarpartByPartNumber(carpart);
			model.addAttribute("message", "Car Part with part # = " + partnumber + " Deleted Successfully");
		} else {
			model.addAttribute("message", "Sorry, a part number is needed in order to delete.");
		}

		return "partnumber";
	}

}
