package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd7Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd7Application.class, args);
	}

}
