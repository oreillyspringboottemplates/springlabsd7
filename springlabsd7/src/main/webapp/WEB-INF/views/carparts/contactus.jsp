<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Contact Us</h1>
<div id="carpartMessage"></div>
<div class="card">
    <div class="cardbody">
        <div class="row">
            <div class="col-sm-12">          <%-- <%=request.getContextPath()%>/carparts/contactus --%>
                <form method="post" id="contactUsForm" action="<c:url value='/carparts/contactus' />">
                    <div class="form-group col-sm-12">
                        <div class="col-sm-5 form-group">
                            <label for="email">From E-mail Address</label>
                            <orly-input id="emailAddress" name="emailAddress" placeholder="E-mail Address"></orly-input>
                        </div>
                        <div class="col-sm-12 form-group">
                            <label for="emailBody" class="mt10">Email Body</label>
                            <orly-input id="emailBody" name="emailBody" placeholder="Enter your message"></orly-input>
                        </div>
                        <div class="col-sm-3 form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
